<?php

return [

    'url' => env('PROGBONUS_API_URL'),

    'token' => env('PROGBONUS_TOKEN'),

    'timeout' => env('PROGBONUS_REQUEST_TIMEOUT'),

    'marketId' => env('PROGBONUS_MARKET_ID'),

    'storeId' => env('PROGBONUS_STORE_ID'),

    'maxWriteOff' => env('PROGBONUS_MAX_WRITEOFF_PERCENT'),

    'bonusPercent' => env('PROGBONUS_BONUS_PERCENT'),

];
