<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/progbonus/{storeId}', 'ProgBonusController@index')->name('progbonus.settings');
Route::get('/api/progbonus/settings', 'ProgBonusController@settings')->name('progbonus.settings');
Route::get('/api/progbonus/bonus-levels', 'ProgBonusController@bonusLevels')->name('progbonus.bonus-levels');
Route::get('/api/progbonus/phones/{phoneNumber}', 'ProgBonusController@getPhone')->name('progbonus.phone');
Route::post('/api/progbonus/registration-code/on/{phoneNumber}', 'ProgBonusController@registrationCode')->name('progbonus.registration-code');
Route::post('/api/progbonus/purchase-code/on/{phoneNumber}/with/{price}', 'ProgBonusController@purchaseCode')->name('progbonus.purchase-code');
Route::post('/api/progbonus/purchases', 'ProgBonusController@savePurchase')->name('progbonus.new-purchase');
Route::post('/api/progbonus/customers', 'ProgBonusController@saveCustomer')->name('progbonus.new-customer');
