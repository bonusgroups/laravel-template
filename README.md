# Laravel On Docker

## UP

`docker-compose exec app php artisan key:generate`
`docker-compose exec app php artisan optimize`

`docker-compose up`

## Dev

`php artisan make:controller XxxController`

## Other usefull Comands

`php artisan config:clear`

## Files

1. ProgBonus Models
2. ProgBonus Service: register as singleton
3. ProgBonus Controller: inject ProgBonus Service
4. routes: /api/ProgBonus
5. config/progbonus.config

## Postman

Check out ProgBonus controller on <https://dark-rocket-1531.postman.co/collections/7154824-638a8c22-1d66-4c2b-a88d-49a5b102d504?workspace=8bdb23bf-062d-4135-8fa6-9913bec849a6>
