<?php

namespace App\Services;

use \Httpful\Exception;

class ProgBonusService
{
    private $PROGBONUS_API_URI;
    private $PROGBONUS_TOKEN;
    private $MARKET_ID;
    private $STORE_ID;
    private $REQUEST_TIMEOUT;

    private $MAX_WRITEOFF;
    private $BONUS_PERCENT;

    private $storeSettings = NULL;
    private $bonusLevels = NULL;

    // The constructor is private
    // to prevent initiation with outer code.
    function __construct()
    {
        $this->PROGBONUS_API_URI = config('progbonus.url');
        $this->PROGBONUS_TOKEN = config('progbonus.token');

        $this->MARKET_ID = config('progbonus.marketId');
        $this->STORE_ID = config('progbonus.storeId');

        $this->MAX_WRITEOFF = config('progbonus.maxWriteOff');
        $this->BONUS_PERCENT = config('progbonus.bonusPercent');

        $this->REQUEST_TIMEOUT = is_null(config('progbonus.timeout')) ? 30 : (int) config('progbonus.timeout');
    }

    public function getStoreSettings(): ProgBonusRequestResult
    {
        if (!is_null($this->storeSettings)) {
            return ProgBonusRequestResult::ok($this->storeSettings);
        }

        try {
            $response = $this->_buildRequest("/api/v1/stores/{$this->STORE_ID}/settings")
                ->send();

            if (!self::_isOk($response)) {
                return ProgBonusRequestResult::fail($response);
            }

            $body = $response->body;
            $this->storeSettings = $body;

            return ProgBonusRequestResult::ok($this->storeSettings);
            // TODO: return new ProgBonusStoreSettings($someObject->firstName, $someObject->phoneNumber);
        } catch (ConnectionErrorException $ce) {
            return ProgBonusRequestResult::exception($ce);
        } catch (Exception $e) {
            return ProgBonusRequestResult::exception($e);
        }
    }

    public function getBonusLevels(): ProgBonusRequestResult
    {
        if (!is_null($this->bonusLevels)) {
            return ProgBonusRequestResult::ok($this->bonusLevels);
            //return $this->bonusLevels;
        }

        try {
            $response = $this->_buildRequest("/api/v1/discount-levels")
                ->send();

            if (!self::_isOk($response)) {
                return ProgBonusRequestResult::fail($response);
            }

            $body = $response->body;
            $this->bonusLevels = $body;
            // TODO: return new ProgBonusBonusLevels();
            return ProgBonusRequestResult::ok($this->bonusLevels);
        } catch (ConnectionErrorException $ce) {
            return ProgBonusRequestResult::exception($ce);
        } catch (Exception $e) {
            return ProgBonusRequestResult::exception($e);
        }
    }

    public function sendRegistrationCode($phoneNumber): ProgBonusRequestResult
    {
        try {
            $url = $this->PROGBONUS_API_URI . "/api/shortcodes/registration/?phoneNumber={$phoneNumber}&storeId={$this->STORE_ID}";
            $response = $this->_signRequest(\Httpful\Request::post($url))->send();

            if (!self::_isOk($response)) {
                return ProgBonusRequestResult::fail($response);
            }

            return ProgBonusRequestResult::ok();
        } catch (ConnectionErrorException $ce) {
            return ProgBonusRequestResult::exception($ce);
        } catch (Exception $e) {
            return ProgBonusRequestResult::exception($e);
        }
    }

    public function sendPurchaseCode($customerId, $price): ProgBonusRequestResult
    {
        $url = $this->PROGBONUS_API_URI . "/api/shortcodes/purchase";

        $reqBosy = [
            "customerId" => $customerId,
            "storeId" => $this->STORE_ID,
            "price" => $price
        ];

        try {
            $response = $this->_signRequest(\Httpful\Request::post($url, json_encode($reqBosy)))
                ->sendsJson()
                ->timeout($this->REQUEST_TIMEOUT)
                ->send();

            // print_r($response);

            if (!self::_isOk($response)) {
                return ProgBonusRequestResult::fail($response);
            }

            return ProgBonusRequestResult::ok();
        } catch (ConnectionErrorException $ce) {
            return ProgBonusRequestResult::exception($ce);
        } catch (Exception $e) {
            return ProgBonusRequestResult::exception($e);
        }
    }

    public function getCustomerByPhone($phoneNumber): ProgBonusRequestResult
    {
        try {
            $response = $this->_buildRequest("/api/v1/customer/by/phone/{$phoneNumber}/?include=bonusInfo,discountLevel")
                ->send();

            if ($response->code == 404) {
                return ProgBonusRequestResult::ok(null);
            }

            if ($response->code != 200) {
                return ProgBonusRequestResult::fail($response);
            }

            $body = $response->body;
            return ProgBonusRequestResult::ok($body);
            // TODO: return new ProgBonusCustomer($someObject->firstName, $someObject->phoneNumber);
        } catch (ConnectionErrorException $ce) {
            return ProgBonusRequestResult::exception($ce);
        } catch (Exception $e) {
            return ProgBonusRequestResult::exception($e);
        }
    }

    public function saveCustomer($data): ProgBonusRequestResult
    {
        $url = $this->PROGBONUS_API_URI . "/api/v1/customers?include=bonusInfo,discountLevel";

        $reqBosy = [
            "phoneNumber" => $data->phoneNumber,
            "fullName" => $data->fullName,
            "shortCode" => $data->shortCode,
            "storeId" => $this->STORE_ID
        ];

        try {
            $response = $this->_signRequest(\Httpful\Request::post($url, json_encode($reqBosy)))
                ->sendsJson()
                ->timeout($this->REQUEST_TIMEOUT)
                ->send();

            if (!self::_isOk($response)) {
                return ProgBonusRequestResult::fail($response);
            }

            $body = $response->body;
            return ProgBonusRequestResult::ok($body);
            // TODO: return new ProgBonusCustomer($someObject->firstName, $someObject->phoneNumber);
        } catch (ConnectionErrorException $ce) {
            return ProgBonusRequestResult::exception($ce);
        } catch (Exception $e) {
            return ProgBonusRequestResult::exception($e);
        }
    }

    public function savePurchase($data): ProgBonusRequestResult
    {
        $url = $this->PROGBONUS_API_URI . "/api/v1/customers/{$data->customerId}/purchases";

        $reqBosy = [
            "price" => $data->price,
            "minus" => $data->minus,
            "shortCode" => $data->shortCode,
            "storeId" => $this->STORE_ID
        ];

        try {
            $response = $this->_signRequest(\Httpful\Request::post($url, json_encode($reqBosy)))
                ->sendsJson()
                ->timeout($this->REQUEST_TIMEOUT)
                ->send();

            if (!self::_isOk($response)) {
                return ProgBonusRequestResult::fail($response);
            }

            $body = $response->body;
            return ProgBonusRequestResult::ok($body);
            // TODO: return new ProgBonusPurchase();
        } catch (ConnectionErrorException $ce) {
            return ProgBonusRequestResult::exception($ce);
        } catch (Exception $e) {
            return ProgBonusRequestResult::exception($e);
        }
    }

    private static function _isOk($response): bool
    {
        return $response->code == 200 || $response->code == 201 || $response->code == 204;
    }

    private function _buildRequest($uri)
    {
        $url = $this->PROGBONUS_API_URI . $uri;
        return $this->_signRequest(\Httpful\Request::get($url)
            ->expectsJson());
    }

    private function _signRequest($req)
    {
        $ts = "PB"; // or rand(1, 10);
        $secret = hash('sha256', $ts . $this->PROGBONUS_TOKEN);

        return  $req->addHeader('market', $this->MARKET_ID)
            ->addHeader('ts', $ts)
            ->addHeader('secret', $secret);
    }
}

class ProgBonusRequestResult
{
    public $isSuccess;
    public $error;
    public $data;

    private function __construct($isSuccess, $error = null, $data = null)
    {
        $this->isSuccess = $error == null && $isSuccess;
        $this->error = $error;
        $this->data = $data;
    }

    public static function ok($data = null)
    {
        return new ProgBonusRequestResult(true, null, $data);
    }

    public static function fail($data)
    {
        return new ProgBonusRequestResult(false, $data->body->message);
    }

    public static function exception($e)
    {
        return new ProgBonusRequestResult(false, $e->getMessage());
    }
}
