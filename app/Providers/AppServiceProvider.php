<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\ProgBonusService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //

        $this->app->singleton(ProgBonusService::class, function () {
            return new ProgBonusService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
