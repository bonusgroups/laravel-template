<?php

namespace App\Http\Controllers;

use App\Services\ProgBonusService;
use Illuminate\Http\Request;

class ProgBonusController extends Controller
{
    private $service;

    function __construct(ProgBonusService $service)
    {
        $this->service = $service;
    }

    //
    public function settings()
    {
        $settings = $this->service->getStoreSettings();
        return response()->json($settings);
    }

    public function bonusLevels()
    {
        $bonusLevels = $this->service->getBonusLevels();
        return response()->json($bonusLevels);
    }

    public function registrationCode($phoneNumber)
    {
        $res = $this->service->sendRegistrationCode($phoneNumber);
        return response()->json($res);
    }

    public function purchaseCode($customerId, $price)
    {
        $res = $this->service->sendPurchaseCode($customerId, $price);
        return response()->json($res);
    }

    public function savePurchase(Request $req)
    {
        $res = $this->service->savePurchase(json_decode($req->getContent()));
        return response()->json($res);
    }

    public function saveCustomer(Request $req)
    {
        $res = $this->service->saveCustomer(json_decode($req->getContent()));
        return response()->json($res);
    }

    public function getPhone($phoneNumber)
    {
        $res = $this->service->getCustomerByPhone($phoneNumber);

        return response()->json($res);
    }
}
